---
title: "Datenauswertung TxKohorte (MONAT JAHR)"
email: "stephan.gloeckner@helmholtz-hzi.de"
output: 
  flexdashboard::flex_dashboard:
    orientation: rows
    vertical_layout: fill
    logo: hzi3.png
---

```{r setup, include=FALSE}
library(tidyverse)
library(lubridate)
library(plotly)
library(flexdashboard)
library(kableExtra)
library(DT)
library(knitr)
library(janitor)
theme_set(theme_light())

path <- "../TxDash_exports"

recent.dir <- list.dirs(path = path) %>% 
  sort(decreasing = TRUE)
recent.path <- recent.dir[1]

files <- list.files(recent.path)
for (i in seq_along(files)) {
  load(paste0(recent.path, "/", files[i]))
}

```

Studienpopulation 
=======================================================================

Row 
-----------------------------------------------------------------------

### Grafik Übersicht Studienpopulation
```{r}
ggplotly(fig01)
```

Row 
-----------------------------------------------------------------------
### Populationstrend nach Zentrum
```{r}
ggplotly(fig02)
```


### Tabelle Übersicht Studienpopulation
```{r}
tab01 %>% 
  kable() %>% 
  kable_styling("striped")
```

### Auf einem Blick
```{r}

```


Bioproben {data-orientation=rows}
=======================================================================

```{r}
blood_value <- sum(df[[1]][[1]]$n)/2
other_value <- sum(df[[2]][[1]]$n)/2
```



Column 
-----------------------------------------------------------------------

### Blutproben

```{r}
articles <- blood_value
valueBox(articles, icon = "ion-waterdrop", color = "#830303")
```

### Andere Proben

```{r}
comments <- other_value
valueBox(comments, icon = "fa-heart", color = "#B07C9E")
```


Column 
-----------------------------------------------------------------------

```{r fig.width=10}
df[[1]][[2]]
```

```{r}
df[[1]][[1]] %>% 
  filter(key == "Organ") %>% 
  select(-key) %>% 
  spread(value, n, fill = 0) %>% 
  adorn_totals() %>% 
  adorn_totals("col") %>% 
  kable() %>% 
  kable_styling("striped")
  
```


Column 
-----------------------------------------------------------------------

```{r fig.width=10}
df[[2]][[2]]
```

```{r}
df[[2]][[1]] %>% 
  filter(key == "Organ") %>% 
  select(-key) %>% 
  spread(value, n, fill = 0) %>% 
  adorn_totals() %>% 
  adorn_totals("col") %>% 
  kable() %>% 
  kable_styling("striped")
```


